def check_rows(g):
    for i in range(0,3):
        if g[i][0] == g[i][1] == g[i][2] and (g[i][2] is not 0) :
        return (True,g[i][0])
    return (False,-1)

def check_cols(g):
    for i in range(0,3):
        if g[0][i] == g[1][i] == g[2][i] and (g[0][i] is not 0):
        return (True,g[0][i])
    return (False,-1)

def check_diag(g):
    if g[0][0] == g[1][1] == g[2][2] and (g[0][0] is not 0):
        return (True,g[0][0])
    if g[0][2] == g[1][1] == g[2][0] and (g[0][2] is not 0):
        return (True,g[0][2])
    return (False,-1)