grades = { "Pero": [2,3,3,4,3,5,3],
           "Djuro" : [4,4,4],
           "Marko" : [3,3,2,3,5,1]
           }

# Ispisi ime studenta koji ima najvisi prosjek. Zadatak treba proci kroz 
# sve studente, izracunati prosjek i ispisati ime studenta s navisim prosjekom.

# -- vas kod ide ispod -- #


def average(dnevn):

  total = float(sum(dnevn))
  return total / len(dnevn)

maxk = None
maxv = None

for k,v in grades.items():
    if maxk is None:
        maxk = k
        maxv = average(v)
    elif average(v) > maxv:
        maxk = k
        maxv = average(v) 

print(maxk)
