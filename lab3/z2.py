# ups: ispisi sve brojeve od 1 do 100 (ukljucujuci i 1 i 100), svaki broj 
# u novi red, ali umjesto broja koji je djeljiv sa 3 ispisi "ups", a umjesto 
# broja koji sadrzi broj 3 ispisi hops
#
# Ukoliko broj i sadrzi 3 i djeljiv je sa 3 (npr. broj 30), ispisi "upshops"


lista = range(1, 101)

for i in lista:
    if i % 3 == 0 and "3" in str(i):
        print("upshops")  
    elif "3" in str(i):
        print("hops")
    elif i % 3 == 0:
        print("ups")  
    else:
        print(i)