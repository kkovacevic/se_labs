# Generated by Django 3.0.2 on 2020-02-02 21:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0002_auto_20200202_1947'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='votes',
            field=models.IntegerField(default=0),
        ),
    ]
