from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Entry, Comment
from .forms import *
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone


class HomeView(ListView):
    model = Entry
    template_name = 'entries/index.html'
    context_object_name = "blog_entries"
    ordering = ['-entry_date']
    paginate_by = 3

class EntryView(DetailView):
    model = Entry
    template_name = 'entries/entry_detail.html'

class CreateEntryView(LoginRequiredMixin, CreateView):
    model = Entry
    template_name = 'entries/create_entry.html'
    fields = ['entry_title', 'entry_text']

    def form_valid(self,form):
        form.instance.entry_author = self.request.user
        return super().form_valid(form)


def comment(request, entry_id):
    entry = get_object_or_404(Entry, pk=entry_id)
    comment = entry.comment_set.create(
        name = request.POST['name'],
        comment = request.POST['comment'],
        pub_date = timezone.now()
    )
    return HttpResponseRedirect(reverse('blog-home'))

def upvote(request, entry_id):
    return cast_vote(request, entry_id, +1)

def downvote(request, entry_id):
    return cast_vote(request, entry_id, -1)

def cast_vote(request, entry_id, score):
    entry = get_object_or_404(Entry, pk=entry_id)
    entry.votes += score
    entry.save()
    return HttpResponseRedirect(reverse('blog-home'))
