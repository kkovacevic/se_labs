from django.contrib import admin

from .models import Entry, Comment

class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'comment')

admin.site.register(Entry)
admin.site.register(Comment, CommentAdmin)
